# Selenium tests tasks

https://www.metric-conversions.org/

## Installation

Download  test-selenium.jar file from:
https://bitbucket.org/alavrent70/tests/downloads/

Get chromedriver for your browser Chrome version:
https://chromedriver.chromium.org/downloads

Put chromedriver.exe and test-selenium.jar to same directory.
## Usage
Run:

java -jar test-selenium.jar

## For repository clone:
https://alavrent70@bitbucket.org/alavrent70/tests.git

