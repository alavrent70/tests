package test_package;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Created by alavrent on 12/2/2019.
 */

public class test_selenium {
    static WebDriver driver = null;
    static WebDriverWait wait = null;
    @BeforeClass
    public static void setup() {
        try {
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            driver = new ChromeDriver(new ChromeDriverService.Builder()
                    .usingDriverExecutable(new File("chromedriver.exe")).build());
        } catch (Exception e){
            e.printStackTrace();
        }


        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 20);
    }
    @Before
    public void setupEach(){
        driver.navigate().to("https://www.metric-conversions.org/");
        driver.manage().window().fullscreen();
    }
    @Test
    public void  celsius_to_Fahrenheit(){
        float arg=45;
        System.out.println("Start Celsius to Fahrenheit test:");
        driver.findElement(By.linkText("Temperature")).click();
        System.out.println("Click on Temperature tab: OK");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement element = driver.findElement(By.linkText("Celsius to Fahrenheit"));
        //((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.perform();
        element.click();
        System.out.println("Choose Celsius to Fahrenheit: OK");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement argumentConv=driver.findElement(By.id("argumentConv"));
        argumentConv.click();
        argumentConv.sendKeys(Float.toString(arg));
        System.out.println("Put Value: " + Float.toString(arg));
        WebElement el=wait.until(ExpectedConditions.presenceOfElementLocated(By.id("answer")));
        System.out.println("Get answer: OK");
        String res = el.getText().split("=")[1].replaceAll("[^0-9?!\\.]","");
        System.out.println(el.getText());
        Assert.assertEquals(arg*1.8+32, Float.valueOf(res), 0.002);
    }
    @Test
    public void  meter_to_feet(){
        float arg=45;
        System.out.println("Start Meter to Feet test:");
        driver.findElement(By.linkText("Length")).click();
        System.out.println("Click on Length tab: OK");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Meters to Feet")));
        //((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.perform();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element.click();
        System.out.println("Choose Meters to Feet: OK");
        WebElement argumentConv=driver.findElement(By.id("argumentConv"));
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        argumentConv.click();
        argumentConv.clear();
        argumentConv.sendKeys(Float.toString(arg));
        System.out.println("Put Value:" + Float.toString(arg));
        WebElement el=wait.until(ExpectedConditions.presenceOfElementLocated(By.id("answer")));
        WebElement dropdown = driver.findElement(By.id("format"));
        dropdown.findElement(By.xpath("//option[. = 'Decimal']")).click();
        System.out.println("Choose decimal answer format: OK");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String res = el.getText().split("=")[1].replaceAll("[^0-9?!\\.]","");
        System.out.println("Get answer: OK");
        System.out.println(el.getText());
        Assert.assertEquals(arg*3.2808, Float.valueOf(res), 0.002);
    }
    @Test
    public void  ounces_to_Grams(){
        float arg=45;
        System.out.println("Start Ounces to Grams test:");
        driver.findElement(By.linkText("Weight")).click();
        System.out.println("Click on Weight tab: OK");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Ounces to Grams")));
        //((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.perform();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element.click();
        System.out.println("Choose Ounces to Grams:");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement argumentConv=driver.findElement(By.id("argumentConv"));
        actions.moveToElement(argumentConv);
        actions.perform();
        argumentConv.click();
        argumentConv.clear();
        argumentConv.sendKeys(Float.toString(arg));
        System.out.println("Put Value:" + Float.toString(arg));
        WebElement el=wait.until(ExpectedConditions.presenceOfElementLocated(By.id("answer")));
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement dropdown = driver.findElement(By.id("format"));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//option[. = 'Decimal']"))).click();
        System.out.println("Choose decimal answer format: OK");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String res = el.getText().split("=")[1].replaceAll("[^0-9?!\\.]","");
        System.out.println("Get answer: OK");
        System.out.println(el.getText());
        Assert.assertEquals(arg/0.035274, Float.valueOf(res), 0.002);
    }
    @AfterClass
    public static void doteardown() {
       driver.close();
    }
}
