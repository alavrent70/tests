package test_package; /**
 * Created by alavrent on 12/6/2019.
 */

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(test_selenium.class);
        for (Failure failure : result.getFailures()) {
           System.out.println(failure.toString());
        }
        //JUnitCore junit = new JUnitCore();
        //Result result = junit.run(test_package.test_selenium.class);
    }
}
